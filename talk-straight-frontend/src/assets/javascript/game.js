
var jumper;
var play = false;
var obstacles = [];
var score = 0;
var oldVolume = 0;
var sensitivity;
var startButton;
var bgImg;
var x1 = 0;
var x2;
var lives = 5;
var gameover = false;

var vocals = ["pa", "ma", "sa", "scha", "sche", "tsch", "ka", "mo"];

var scrollSpeed = 1;

function setup() {

  canvasDiv = document.getElementById("game");

  canvas = createCanvas(canvasDiv.offsetWidth, canvasDiv.offsetHeight);
  canvas.parent("gameCanvas");
  bgImg = loadImage("assets/images/background.png");
  x2 = width;
  fade = 255;
  fadeVocal = 255;

  showPlusTen = false;
  showVocal = false;
  startButton = document.getElementById("startButton");
  sensitivity = document.getElementById("customRange");
  sensitivityLbl = document.getElementById("customRangeLabel");
  hearts = document.getElementById("heartContainer");
  liveLabel = document.getElementById("liveLabel");
  gameoverContainer = document.getElementById("gameOver");
  stats = document.getElementById("statsContainer")
  scoreTextfield = document.getElementById("scoreText");
} 


function start() {
  //activate game
  play = true;
  startButton.remove();

  //start the audio input
  mic = new p5.AudioIn();
  mic.start();
  getAudioContext().resume();

  //initialize all objects needed for the game
  jumper = new Jumper();

  sensitivity.style.display = "block";
  sensitivityLbl.style.display="block";
  liveLabel.style.display = "block";
  hearts.style.display = "block";
  stats.style.display = "block";


}

function draw() {
  drawBackground();

  if(gameover){
    showGameOver();
    return;
  }
  
  //only play if the playbutton was pressed
  if (play == false) {
    return;
  }


  //draw the ball
  jumper.show();
  jumper.update();

  //drawing the score
  drawScore();

  //if score is incremented show +10
  if(showPlusTen){
    textSize(120);
    fill(255, 255, 255, fade)
    text("+10", width / 2 -100, height / 2)
    fade -= 5; 
    if(fade < 0){
      showPlusTen = false;
      fade = 255;
    }
  }

  if(showVocal){
    textSize(120);
    fill(255, 255, 255, fadeVocal)
    text(vocal.toUpperCase(), width / 2 -100, height / 2)
    fadeVocal -= 3; 

    if(fadeVocal < 0){
      showVocal = false;
      fadeVocal = 255;
    }

  }

  //insert an obstacle all 120 frames
  if (frameCount % 230 == 0) {
    obstacles.push(new Obstacle());
  }


  for (var i = obstacles.length - 1; i >= 0; i--) {
    obstacles[i].show();
    obstacles[i].update();

    if(obstacles[i].jumperAtObstacle(jumper)){
      vocal = vocals[getRandomInt(0, vocals.length-1)];
      showVocal = true;
    }

    //jumper passed the obstacle
    if (obstacles[i].jumperPassedObstacle(jumper)) {
      //and did not touch the obstacle
    
      if (!obstacles[i].wasAlreadyTouched()) {
        showPlusTen = true;
        score += 10;
      }
    }

    //jumper touched the obstacle (the first time) 
    if (!obstacles[i].wasAlreadyTouched() && obstacles[i].touches(jumper)) {
      if (this.score < 0) {
        this.score = 0; 
      }

      lives -=1;

      updateHearts();

      if(lives == 0){
        gameover = true;
      }

    }
    //if the obstacle is off the screen remove it from the obstacle list
    if (obstacles[i].offScreen()) {
      obstacles.splice(i, 1);
    }
  }

  var threshold = sensitivity.value;
  var volume = mic.getLevel();

  if (volume > threshold) {
    jumper.up();

  }

  //draw volume
  var thresholdy = map(threshold, 0, 1, height, 0)
  stroke(255, 0, 0);
  strokeWeight(4);
  line(width - 50, thresholdy, width, thresholdy)
  oldVolume = volume;
  fill(0, 255, 0);
  noStroke();
  var y = map(volume, 0, 1, height, 0);
  rect(width - 50, y, 50, height - y)

}

function drawScore() {
  margin = 120;
  textSize(25);
  text("Score: " + score, width - margin, 50);

}

function updateHearts(){
  var children = this.hearts.childNodes;
  for(i=1; i < children.length; i++){

    if(i>this.lives){
      children[i].style.display="none";
    } 
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


function drawBackground() {

  //scrolling the background
  image(bgImg, x1, 0, width, height);
  image(bgImg, x2, 0, width, height);

  x1 -= scrollSpeed;
  x2 -= scrollSpeed;

  if (x1 < -width) {
    x1 = width;
  }
  if (x2 < -width) {
    x2 = width;
  }

}

function showPlusTen() {
  fadeAmount = -1;
  fade = 255;
  textSize(100);

  while (fade > 0) {
    fill(255, 255, 255, fade)
    text("+10", width / 2, height / 2)
    fade -= 1;
  }
}

function keyPressed() {
  if (key == ' ') {
    jumper.up();
    jumper.show();
  }

}

function replay(){
 this.lives = 5;
 gameover = false;
 play = true;
 gameoverContainer.style.display = "none";
 stats.style.display = "block";
 obstacles = [];

 var children = this.hearts.childNodes;   

 for(i=1; i < children.length; i++){
       children[i].style.display="inline";
  } 

}

function showGameOver(){
  gameoverContainer.style.display = "block";  
  stats.style.display = "none";
  scoreTextfield.textContent="Score: "+ score;

}

