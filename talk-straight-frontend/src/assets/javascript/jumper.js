function Jumper() {
    this.y = height/2; //place jumper in the middle of the height
    this.x = 100;
    this.jumpingForce = -15;
    this.gravity = 0.4;
    this.velocity = 0;
    this.show = function() { //draw the jumper (currently a circle)
        noStroke();
        fill(255);
        ellipse(this.x, this.y, 32,32);
    }
  
    this.update = function(){
        this.velocity += this.gravity;
        this.y += this.velocity;
        if (this.y > height){    
            this.y = height;
            this.velocity = 0;
        }
        if (this.y < 0){
            this.y = 0;
            this.velocity = 0;
        }
    }

    this.up = function(){
        this.velocity += this.jumpingForce;
        this.y += this.velocity; 

    }

    
}