function Obstacle() {
    this.space = random(100, height/1.5);
    this.spaceCenter = random(this.space+10, height-(this.space+10));   
    this.wasTouched = false;
    this.jumperPassed = false;
    this.wasNotInSight = true;

    this.top = this.spaceCenter - this.space/2;
    this.bottom = height - this.spaceCenter - this.space/2;
    this.x = width;  
    this.w = random(25,50);      
    this.speed = 2;

    this.highlight = false;

    this.show = function(){
        noStroke();
        if(this.highlight){
         fill(0,0,255);
        } else {
         fill(255);
        }
        rect(this.x, 0, this.w, this.top);
        rect(this.x, height-this.bottom, this.w, this.bottom);
    }

    this.wasAlreadyTouched = function(){
        return this.wasTouched;
    }

    this.update = function(){
        this.x -= this.speed;
    }

    this.offScreen = function(){
        if( this.x < -this.w){ //when the wohle width of the obstacle is off the screen   
            return true;
        } else {
            return false;
        }
    }

    this.touches = function(jumper){
        if((jumper.y <= this.top || jumper.y >= height-this.bottom) && jumper.x >= this.x && jumper.x <= this.x+this.w){
            this.highlight = true;
            this.wasTouched = true;
            return true;
        } else {
            this.highlight = false;
            return false;
        }  

    }

    this.jumperAtObstacle = function(jumper){
        if(this.x < 300 && this.wasNotInSight){
            this.wasNotInSight = false;
            return true;
        } else {
            return false;
        }


    }

    this.jumperPassedObstacle = function(jumper){
        if(!this.jumperPassed && jumper.x > this.x+this.w){
            this.jumperPassed = true;
            return true;
        }
        return false;
    }


}