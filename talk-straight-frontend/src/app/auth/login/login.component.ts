import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthServiceService } from '../auth-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../models/user/user.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  submitted = false;
  userChanged = new Subject<User>();
  hide = true;
  formGroupLogin: FormGroup;
  retrievedImage: any;
  retrieveRes: any;
  constructor(public authService: AuthServiceService,
              private modalService: NgbModal,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log('inside init');
    this.initForm();
  }

  initForm() {
    this.formGroupLogin = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')])
      , password: new FormControl('', [Validators.required])
    });
  }

  getErrorMessage(type: string) {
    if (type === 'email') {
      return this.formGroupLogin.get('email').hasError('required') ? 'Email is required' :
      this.formGroupLogin.get('email').hasError('pattern') && this.formGroupLogin.get('email').hasError('email') ?
        'Please enter a valid email' :
          '';
    } else {
      return this.formGroupLogin.get('password').hasError('required') ? 'Password is required' : '';
    }
  }

  loginForm() {
    this.submitted = true;
    console.log('inside submit');
    if (this.formGroupLogin.valid) {
      this.authService.login(this.formGroupLogin.value).subscribe(result => {
        if (this.authService.getError() !== 'no user') {
          this.user = result;
          this.userChanged.subscribe(u => this.user = u);
          console.log(this.user);
          localStorage.setItem('name', this.user.name);
          localStorage.setItem('email', this.user.email);
          localStorage.setItem('id', this.user.id);
          localStorage.setItem('about', this.user.about);
          // this.retrieveRes = result;
          // //console.log(this.retrieveRes.picByte);
          // this.retrievedImage = `data:image/(png|jpg|jpeg);base64,${this.retrieveRes.picByte}`;
          // //console.log(this.retrievedImage);
          // localStorage.setItem('pic', this.retrievedImage);
          this.authService.toggleToken();
          localStorage.setItem('token', this.authService.getToken());
          console.log(localStorage.getItem('token'));
          this.resetForm();
          this.router.navigate(['/'], { relativeTo: this.route });
        }
        // this.authService.recieveUserData(this.user);
      });
    } else {
      return;
     // alert('Fill required detail!');
    }
  }

  resetForm() {
    this.formGroupLogin.reset();
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true, size: 'lg', });
  }

  // convenience getter for easy access to form fields
  get f() { return this.formGroupLogin.controls; }

}
