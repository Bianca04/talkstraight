import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';
import { NgForm, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { User } from '../../models/user/user.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  regBtnClicked: boolean;
  otp: FormGroup;
  registerForm: FormGroup;
  submitted = false;
  hide = true;
  formErrors: any;

  constructor(private authService: AuthServiceService,
              private router: Router, private route: ActivatedRoute,
              private builder: FormBuilder) { }

  onFormValuesChanged() {
    for ( const field in this.formErrors ) {
            if ( !this.formErrors.hasOwnProperty(field) ) {
                continue;
            }
            // Clear previous errors
            this.formErrors[field] = {};
            // Get the control
            const control = this.registerForm.get(field);
            if ( control && control.dirty && !control.valid ) {
                this.formErrors[field] = control.errors;
            }
        }
  }

  ngOnInit(): void {
    console.log('inside init');
    this.regBtnClicked = false;
    this.initForm();
  }

  initForm() {
    this.registerForm = this.builder.group({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });

    this.otp = new FormGroup({
      otp: new FormControl('', [Validators.required])
    });

    this.registerForm.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });
  }

  getErrorMessage(type: string) {
    if (type === 'email') {
      return this.registerForm.get('email').hasError('required') ? 'Email is required' :
      this.registerForm.get('email').hasError('pattern') && this.registerForm.get('email').hasError('email') ?
        'Please enter a valid email' :
          '';
    } else {
      return this.registerForm.get('password').hasError('required') ? 'Password is required' :
      this.registerForm.get('password').hasError('minlength') ? 'Password must contain at least 6 characters' :
          '';
    }
  }

  submitUserForm() {
    this.submitted = true;
    console.log('inside submit');
    if (this.registerForm.valid && this.regBtnClicked === false) {
      this.authService.register(this.registerForm.value).subscribe();
      if (this.authService.getError() !== 'email error') {
        this.regBtnClicked = true;
      }
      // this.resetForm();
      // alert('Thank You for registering');
    } else {
      return;
    }
  }

  onVerifyOtp() {
    this.authService.verifyOtp(this.otp.value, this.registerForm.value).subscribe(result => {
      console.log(result);
      // tslint:disable-next-line:prefer-const
      let userId;
      localStorage.setItem(userId, result.id);
      this.user = result;
      console.log(this.user);
      this.resetForm();
      alert('Thank you ' + this.user.name + ' for registering. Now you can login.');
      this.router.navigateByUrl('/login');
    });
  }

  resetForm() {
    this.registerForm.reset();
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
}
