import { AfterContentInit, Component, OnInit, AfterViewInit,AfterViewChecked } from '@angular/core';

@Component({
  selector: 'app-game-overview',
  templateUrl: './game-overview.component.html',
  styleUrls: ['./game-overview.component.css']
})
export class GameOverviewComponent implements AfterViewChecked {

  loadAPI: Promise<any>;

  lives=5;
  score = 0;

  constructor() { }



  ngAfterViewChecked() {
    this.loadAPI = new Promise((resolve) => {
      this.loadScript("https://cdnjs.cloudflare.com/ajax/libs/p5.js/1.2.0/addons/p5.sound.js");
      this.loadScript("./assets/javascript/game.js");
      this.loadScript("./assets/javascript/jumper.js");
      this.loadScript("./assets/javascript/obstacles.js");

    });
  }

 getScore(){
  this.score = window["score"];
  console.log(this.score);
}


  public loadScript(url: string) {
    // const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', url);
    document.getElementsByTagName('head')[0].appendChild(script);
  }


}
