import { Injectable } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import * as moment from 'moment';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {DomSanitizer} from "@angular/platform-browser";
import {User} from "../models/user/user.model";
import {catchError} from "rxjs/operators";
import {Category} from "./model/category.model";

interface RecordedAudioOutput {
  blob: Blob;
  title: string;
}

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  private stream;
  private recorder;
  private interval;
  private startTime;
  private _recorded = new Subject<RecordedAudioOutput>();
  private _recordingTime = new Subject<string>();
  private _recordingFailed = new Subject<string>();
  fileUrl;
  mp3Name;


  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  getRecordedBlob(): Observable<RecordedAudioOutput> {
    return this._recorded.asObservable();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  recordingFailed(): Observable<string> {
    return this._recordingFailed.asObservable();
  }


  startRecording() {

    if (this.recorder) {
      // It means recording is already started or it is already recording something
      return;
    }

    this._recordingTime.next('00:00');
    navigator.mediaDevices.getUserMedia({ audio: true }).then(s => {
      this.stream = s;
      this.record();
    }).catch(error => {
      this._recordingFailed.next();
    });

  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {

    this.recorder = new RecordRTC.StereoAudioRecorder(this.stream, {
      type: 'audio',
      mimeType: 'audio/webm'
    });

    this.recorder.record();
    this.startTime = moment();
    this.interval = setInterval(
      () => {
        const currentTime = moment();
        const diffTime = moment.duration(currentTime.diff(this.startTime));
        const time = this.toString(diffTime.minutes()) + ':' + this.toString(diffTime.seconds());
        this._recordingTime.next(time);
      },
      1000
    );
  }

  private toString(value) {
    let val = value;
    if (!value) {
      val = '00';
    }
    if (value < 10) {
      val = '0' + value;
    }
    return val;
  }

  stopRecording() {

    if (this.recorder){
      console.log('media stopped');
      this.recorder.stop((blob) => {
        if (this.startTime) {
          this.mp3Name = encodeURIComponent('audio_' + new Date().getTime() + '.mp3');
          this.stopMedia();
          this._recorded.next({ blob: blob, title: this.mp3Name });

          console.log('set url');
          this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl
          (window.URL.createObjectURL(blob));
          console.log(this.fileUrl);
        }
      }, () => {
        this.stopMedia();
        this._recordingFailed.next();
      });
    }
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval);
      this.startTime = null;
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream = null;
      }
    }
  }

  uploadByCategory(file: File, category): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    //formData.append('category', category);
    console.log(category);

    const req = new HttpRequest('POST', `${this.baseUrl}/uploadCategory/${category}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/files`);
  }

  getFilesbyCategory(category): Observable<any> {
    return this.http.get(`${this.baseUrl}/filesCategory/${category}`);
  }

  getAllCategories(): Observable<Category[]>{
      return this.http.get<Category[]>(this.baseUrl + '/categories');
  }
}
