import {Component, OnDestroy, OnInit} from '@angular/core';
import { UploadFileService } from './upload-file.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import {Category} from './model/category.model';
import {FileOfCat} from './model/fileofcat.model';
import { MatDialog, MatDialogRef, ThemePalette } from '@angular/material';

export interface Recording {
  title: string;
  date: string;
  category: string;
  color: string;
}

@Component({
  selector: 'upload',
  templateUrl: './upload-files.component.html',
  styles: ['./upload-files.component.css']
})
export class UploadFilesComponent implements OnInit {

  message = '';

  categories: Category[];
  filesOfCategory: FileOfCat[];

  recordings: Recording[] = [];
  cardColor: string;

  colors = ['EF5350', 'EC407A', 'BA68C8', '42A5F5', '00BCD4', '66BB6A', '9CCC65', 'FFEE58',
          'FFA726', 'FF7043', '90A4AE'];

  constructor(private uploadService: UploadFileService, private sanitizer: DomSanitizer,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.getFilesAndCategories();
  }

  getFilesAndCategories() {
    console.log ('init');
    this.uploadService.getAllCategories().subscribe(
      (result: Array<Category>) => {
        console.log('success', result);
        this.categories = result;

        if (this.categories.length > 0) {
          console.log('success', this.categories[0]);
          this.setRecordings();
        }
      },
      (error: any) => {
        console.log('error', error);
      }
    );
  }

  setRecordings() {
    let index = 0;
    this.recordings = [];
    for (const cat of this.categories) {
      console.log(cat.name)
      this.cardColor = this.colors[index];

      this.uploadService.getFilesbyCategory(cat.name).subscribe(
        (result: Array<FileOfCat>) => {
          console.log('success', result);
          this.filesOfCategory = result;

          if (this.filesOfCategory.length > 0) {
            console.log('success', this.filesOfCategory[0]);

            this.filesOfCategory.forEach(elementFile => {
              let fileName = elementFile.name;
              const timeIndex = fileName.lastIndexOf('_');
              fileName = fileName.substring(0, fileName.lastIndexOf('.'));

              let date;
              if (fileName.includes('_')) {
                fileName = fileName.substring(0, fileName.indexOf('_'));

                const timestamp = elementFile.name.substring(timeIndex + 1);

                date = new Date(+timestamp.substring(0, 4),
                +timestamp.substring(4, 6) - 1,
                +timestamp.substring(6, 8),
                +timestamp.substring(8, 10),
                +timestamp.substring(10, 12),
                +timestamp.substring(12, 14)).toLocaleString();
              } else {
                date = new Date().toLocaleString();
              }

              this.recordings.push({title: fileName,
                date,
                category: cat.name,
                color: this.cardColor
              });
              });
        }
        },
        (error: any) => {
          console.log('error', error);
        }
      );

      index++;
    }
  }

  getCatColor(category: string): string {
    let color = '#';
    if (category === 'A-Laute') {
      color += this.colors[10];
    } else if (category === 'B-Laute') {
      color += this.colors[9];
    } else if (category === 'C-Laute') {
      color += this.colors[8];
    } else if (category === 'S-Laute') {
      color += this.colors[7];
    } else if (category === 'H-Laute') {
      color += this.colors[6];
    } else {
      const index = Math.floor(Math.random() * 5);
      color += this.colors[index];
    }

    return color;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewDialogComponent, {
      width: '800px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getFilesAndCategories();
    });
  }
}

/** Upload Dialog */
@Component({
  selector: 'app-dialog-overview-dialog',
  templateUrl: 'dialog-overview.html',
  styleUrls: ['upload-files.component.css']
})
export class DialogOverviewDialogComponent implements OnDestroy {

  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';

  isRecording = false;
  recordedTime;
  blobUrl;
  record;
  category = '';
  recordingName = '';

  constructor (
    public dialogRef: MatDialogRef<DialogOverviewDialogComponent>,
    private sanitizer: DomSanitizer,
    private uploadService: UploadFileService) {
      this.uploadService.recordingFailed().subscribe(() => {
        this.isRecording = false;
      });

      this.uploadService.getRecordedTime().subscribe((time) => {
        this.recordedTime = time;
      });

      this.uploadService.getRecordedBlob().subscribe((data) => {
        this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob));
        this.record = data.blob;
        this.record.lastModified = new Date();
        this.record.name = 'recording' +
        '_' + this.record.lastModified.getFullYear() +
        String(this.record.lastModified.getMonth() + 1).padStart(2, '0') +
          String(this.record.lastModified.getDay()).padStart(2, '0') +
          String(this.record.lastModified.getHours()).padStart(2, '0') +
          String(this.record.lastModified.getMinutes()).padStart(2, '0') +
          String(this.record.lastModified.getSeconds()).padStart(2, '0') + '.mp3';
      });
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  startRecording() {
    if (!this.isRecording) {
      this.isRecording = true;
      this.uploadService.startRecording();
    }
  }

  abortRecording() {
    if (this.isRecording) {
      this.isRecording = false;
      this.uploadService.abortRecording();
      this.record = null;
    }
  }

  stopRecording() {
    if (this.isRecording) {
      this.uploadService.stopRecording();
      this.isRecording = false;
    }
  }

  clearRecordedData() {
    this.blobUrl = null;
  }

  ngOnDestroy(): void {
    this.abortRecording();
  }
//--------
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }


  uploadbyCategory() {

    //neues bzw. zu verwendendes category durch User bestimmt

    this.progress = 0;
    console.log('upload by category started');
    this.currentFile = this.selectedFiles.item(0);
    let splitter = this.currentFile.name.split('.');
    let currentDate = new Date();
    this.currentFile = new File([this.currentFile], splitter[0] + '_' + currentDate.getFullYear() +
      String(currentDate.getMonth()+1).padStart(2, '0') + String(currentDate.getDay()).padStart(2, '0') +
      String(currentDate.getHours()).padStart(2, '0') + String(currentDate.getMinutes()).padStart(2, '0') +
      String(currentDate.getSeconds()).padStart(2, '0') + '.' + splitter[1], {type: this.currentFile.type});

    this.upload(this.currentFile);

    this.selectedFiles = undefined;

  }

  uploadRecording() {
    console.log('Upload started for Recording');
    if (this.recordingName === ''){
      this.recordingName = 'recording';
    }
    this.record.name = this.recordingName + '_' + this.record.name;
    this.upload(this.record);
  }

  async upload(file) {
    this.category = (document.getElementById('category') as HTMLInputElement).value;
    if (this.category === '') {
      this.category = 'undefined';
    }

    this.uploadService.uploadByCategory(file, this.category).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
        }
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = undefined;
      });
    await this.uploadService.uploadByCategory(file, this.category).toPromise().then(x => {
      console.log(x);
      this.closeDialog();
    }, err => {
      this.closeDialog();
    });
  }

  closeDialog(){
    this.clearRecordedData();
    this.dialogRef.close();
  }

}
