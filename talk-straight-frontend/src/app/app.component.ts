import { Component } from '@angular/core';
import { AppService } from './app.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title: string;
  constructor(private appService: AppService) {
    this.title = 'talk-straight-frontend';
  }

  records: any[] = [];

  destroy$: Subject<boolean> = new Subject<boolean>();

  test() {
    this.appService.getRecords().pipe(takeUntil(this.destroy$)).subscribe((records: any[]) => {
      this.records = records;
    });
    console.log('Success');
  }

}
