import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { NavbarComponent } from './navbar/navbar.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {VideochatComponent} from './videochat/videochat.component';
import { GameOverviewComponent } from './game-overview/game-overview.component';
import {SearchComponent} from './search/search.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {NgbAccordionModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {VideocallComponent} from './videochat/videocall.component';
import { ChartsModule, ThemeService } from 'ng2-charts';
import {DialogOverviewDialogComponent, UploadFilesComponent} from './file-upload/upload-files.component';
import { AnalogClockComponent } from './analog-clock/analog-clock.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AchievementsComponent,
    DashboardComponent,
    VideochatComponent,
    SearchComponent,
    LoginComponent,
    RegisterComponent,
    VideocallComponent,
    GameOverviewComponent,
    UploadFilesComponent,
    AnalogClockComponent,
    DialogOverviewDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NoopAnimationsModule,
    NgbAlertModule,
    NgbAccordionModule,
    ChartsModule
  ],
  entryComponents: [DialogOverviewDialogComponent],
  providers: [ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
