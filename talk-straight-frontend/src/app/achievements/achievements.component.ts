import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { AchievementsService } from '../achievements/achievements.service';
import * as myGlobals from '../globals';

export interface Goal {
  title: string;
  description: string;
  icon: string;
  achieved: boolean;
}

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent implements OnInit {
  levels = myGlobals.levels;
  specialRewards = myGlobals.specialRewards;
  totalLevels = myGlobals.totalLevels;
  totalSpecialRewards = myGlobals.totalSpecialRewards;
  levelsAchieved = myGlobals.levelsAchieved;
  specialsAchieved = myGlobals.specialRewardsAchieved;
  pointScore = myGlobals.pointScore;
  grayFilter = 'background-color: darkgray; color:black; filter: blur(3px) grayscale(100%);';

  levelUpPoints: number;
  levelUpProgress: number;
  totalRewards: number;
  rewardsAchieved: number;
  currentLevel: string;

  goals: Goal[] = [
    {
      title: 'Uploaded 1 recording today',
      description: 'x out of 1 recording uploaded. Play x to reach this goal!',
      icon: 'mic',
      achieved: true
    },
    {
      title: 'Uploaded 5 recordings this week',
      description: 'x out of 1 recording uploaded. Play x to reach this goal!',
      icon: 'mic',
      achieved: true
    },
    {
      title: 'Played 3 rounds this week',
      description: 'x out of 3 rounds played. Play x to reach this goal!',
      icon: 'videogame_asset',
      achieved: false
    },
    /*{
      title: 'Checked 2 reviewed recordings this week',
      description: 'x out of 2 recordings checked. Check x to reach this goal!',
      icon: 'fact_checked',
      achieved: false
    },*/
    {
      title: 'Played 10 rounds this week',
      description: 'x out of 10 rounds played. Play x to reach this goal!',
      icon: 'videogame_asset',
      achieved: false
    }
  ];

  public lineChartData: ChartDataSets[] = [
    { data: [2, 1, 0, 1, 2], label: 'Recordings uploaded' },
    { data: [3, 1, 2, 1, 1], label: 'Games played' },
    { data: [20, 10, 15, 8, 10], label: 'Points received', yAxisID: 'y-axis-1'}
  ];
  public lineChartLabels: Label[] = this.achievementsService.getLabelStatistics();
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left'
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {}
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderWidth: 2,
          label: {
            enabled: true,
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // accent
      backgroundColor: 'rgba(0,131,143,0.2)',
      borderColor: 'rgba(0,131,143,0.2)',
      pointBackgroundColor: 'rgba(0,131,143,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(0,131,143,0.8)'
    },
    { // orange
      backgroundColor: 'rgba(255,112,67,0.2)',
      borderColor: 'rgba(255,112,67,1)',
      pointBackgroundColor: 'rgba(255,112,67,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(255,112,67,0.8)'
    },
    { // yellow
      backgroundColor: 'rgba(253,216,53,0.2)',
      borderColor: 'rgba(253,216,53,1)',
      pointBackgroundColor: 'rgba(253,216,53,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(253,216,53,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(private achievementsService: AchievementsService) {
    this.totalRewards = this.totalLevels + this.totalSpecialRewards;
    this.rewardsAchieved = this.levelsAchieved + this.specialsAchieved;
  }

  ngOnInit() {
    this.setLevelStatus();
    this.setSpecialRewardsStatus();

    const levelUpProgress = this.achievementsService.getLevelUpProgress();
    this.currentLevel = this.achievementsService.getCurrentLevel();
    this.levelUpPoints = levelUpProgress[0];
    this.levelUpProgress = levelUpProgress[1];
  }

  setLevelStatus() {

    for (let index = this.levelsAchieved + 1; index <= this.totalLevels; index++) {
      const levelElement = document.getElementById('level' + index);
      levelElement.setAttribute('style', this.grayFilter);
    }
  }

  setSpecialRewardsStatus() {

    for (let index = this.specialsAchieved + 1; index <= this.totalSpecialRewards; index++) {
      const specialElement = document.getElementById('specialReward' + index);

      specialElement.setAttribute('style', this.grayFilter);
    }
  }

  /*
  setCurrentLevel() {
    let minPoints = 0;
    let maxPoints = 10;

    if (this.pointScore >= 0 && this.pointScore < 10) {
      this.currentLevel = '-';
    } else if (this.pointScore >= 10 && this.pointScore < 100) {
      this.currentLevel = this.levels[0];
      minPoints = 10;
      maxPoints = 100;
    } else if (this.pointScore >= 10 && this.pointScore < 200) {
      this.currentLevel = this.levels[1];
      minPoints = 100;
      maxPoints = 200;
    } else if (this.pointScore >= 200 && this.pointScore < 400) {
      this.currentLevel = this.levels[2];
      minPoints = 200;
      maxPoints = 400;
    } else if (this.pointScore >= 400) {
      this.currentLevel = this.levels[3];
      maxPoints = -1;
    }

    this.levelUpPoints = maxPoints === -1 ? -1 : maxPoints - this.pointScore;
    this.levelUpProgress = this.levelUpPoints === -1 ? 100 : ((this.pointScore - minPoints) / (maxPoints - minPoints)) * 100;
  }
  */

  isLevelUp(): boolean {
    return this.pointScore < 400;
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}
