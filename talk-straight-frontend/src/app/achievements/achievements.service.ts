import { Injectable } from '@angular/core';
import * as myGlobals from '../globals';

@Injectable({
  providedIn: 'root'
})
export class AchievementsService {

  pointScore = myGlobals.pointScore;
  levels = myGlobals.levels;
  currentLevel: string;

  constructor() { }

  getLevelUpProgress(): number[] {
    let minPoints = 0;
    let maxPoints = 10;

    if (this.pointScore >= 0 && this.pointScore < 10) {
      this.currentLevel = '-';
    } else if (this.pointScore >= 10 && this.pointScore < 100) {
      this.currentLevel = this.levels[0];
      minPoints = 10;
      maxPoints = 100;
    } else if (this.pointScore >= 10 && this.pointScore < 200) {
      this.currentLevel = this.levels[1];
      minPoints = 100;
      maxPoints = 200;
    } else if (this.pointScore >= 200 && this.pointScore < 400) {
      this.currentLevel = this.levels[2];
      minPoints = 200;
      maxPoints = 400;
    } else if (this.pointScore >= 400) {
      this.currentLevel = this.levels[3];
      maxPoints = -1;
    }

    const levelUpPoints = maxPoints === -1 ? -1 : maxPoints - this.pointScore;
    const levelUpProgress = levelUpPoints === -1 ? 100 : ((this.pointScore - minPoints) / (maxPoints - minPoints)) * 100;

    return [levelUpPoints, levelUpProgress];
  }

  getCurrentLevel(): string {
    return this.currentLevel;
  }

  getLabelStatistics(): string[] {
    const date = new Date();
    const month = date.getMonth();

    const labels = [];
    let startMonth = month - 1;

    for (let index = 5; index > 0; index--) {
      if (startMonth < 0) {
        startMonth = 11;
      }

      labels.push(this.getMonthNames(startMonth--));
    }

    return labels.reverse();
  }

  getMonthNames(month: number): string {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

    return monthNames[month];
  }
}
