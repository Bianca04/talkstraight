import { Component, OnInit } from '@angular/core';
import {AuthServiceService} from "../auth/auth-service.service";
import {SearchService} from "../search/search.service";
import {ActivatedRoute} from "@angular/router";
import {Observable, Subject} from "rxjs";
import {User} from "../models/user/user.model";
import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  constructor(public auth: AuthServiceService, private route: ActivatedRoute) { }
  ngOnInit(): void {
  }

  onLogout(){
    localStorage.removeItem('name');
    localStorage.removeItem('email');
    localStorage.removeItem('id');
    this.auth.logout();
  }
}
