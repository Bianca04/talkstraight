import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AchievementsComponent } from './achievements/achievements.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {VideochatComponent} from './videochat/videochat.component';
import {GameOverviewComponent} from './game-overview/game-overview.component';
import {RegisterComponent} from './auth/register/register.component';
import {LoginComponent} from './auth/login/login.component';
import {VideocallComponent} from './videochat/videocall.component';
import {UploadFilesComponent} from './file-upload/upload-files.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'recordings', component: UploadFilesComponent },
  { path: 'chat', component: VideochatComponent },
  { path: 'achievements', component: AchievementsComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'videocall/:id', component: VideocallComponent},
  { path: 'game', component: GameOverviewComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
