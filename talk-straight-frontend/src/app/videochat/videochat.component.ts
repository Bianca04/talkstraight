/*import {Component, OnInit, ViewChild } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'videochat',
  templateUrl: './videochat.component.html',
 // styleUrls: ['./videochat.component.css']
})

export class VideochatComponent implements  OnInit {
  title = 'video';

  @ViewChild('videoElement', {static: true}) videoElement: any;

  peer;
  anotherId;
  myPeerId;

  ngOnInit() {
    const video = this.videoElement.nativeElement;
    this.peer = new Peer();
    setTimeout(() => {
      this.myPeerId = this.peer.id;
      console.log(this.myPeerId);
    }, 3000);
    // tslint:disable-next-line:only-arrow-functions
    this.peer.on('connection', function(conn) {
      console.log('testicon:');
    // tslint:disable-next-line:only-arrow-functions
      conn.on('data', function(data) {
        console.log('testicon2:');
        console.log(data);
      });
    });

    const n = navigator as any;
    n.getUserMedia = n.getUserMedia ||
      n.webkitGetUserMedia ||
      n.mozGetUserMedia;


    if (n.getUserMedia) {}
    {
      // tslint:disable-next-line:only-arrow-functions
      this.peer.on('call', function(call) {
        console.log('testi:');
        // let temp = n.mediaDevices.getUserMedia;
        // tslint:disable-next-line:only-arrow-functions
        n.getUserMedia({video: true, audio: true}, function(stream) {
          call.answer(stream);
          // tslint:disable-next-line:only-arrow-functions
          call.on('stream', function(remotestream) {
            console.log('test:');
            // video.src = URL.createObjectURL(remotestream)
            video.srcObject = remotestream;
            console.log('src:');
            console.log(video.src);
            video.muted = false;
            video.play();
          });
        // tslint:disable-next-line:only-arrow-functions
        }, function(err) {
          console.log('Failed to get stream', err);
        });
      });
    }
  }

  connect() {
    const conn = this.peer.connect(this.anotherId);
    // on open will be launch when you successfully connect to PeerServer
    // tslint:disable-next-line:only-arrow-functions
    conn.on('open', function() {
      // here you have conn.id
      conn.send('message form that');
    });
  }

  videoConnect() {
    const video = this.videoElement.nativeElement;
    console.log('con' + this.videoElement);
    console.log('con ' + video);
    // var video = document.querySelector('#videoElement');
    const localvar = this.peer;
    const fname = this.anotherId;

    const n = navigator as any;
    n.getUserMedia = n.getUserMedia ||
      n.webkitGetUserMedia ||
      n.mozGetUserMedia;


    if (n.getUserMedia) {}{
      console.log('testi:tut1');
      // tslint:disable-next-line:only-arrow-functions
      n.getUserMedia({video: true, audio: true}, function(stream) {

        const call = localvar.call(fname, stream);
        // tslint:disable-next-line:only-arrow-functions
        call.on('stream', function(remotestream) {

          console.log('testi:tut');
          // video.src = URL.createObjectURL(remotestream);
          video.srcObject = remotestream;
          console.log('src:');
          console.log(video.src);
          video.play();
          video.muted = true;
        });
      // tslint:disable-next-line:only-arrow-functions
      }, function(err) {
        console.log('Failed to get stream', err);
      });
    }
  }

  videoOff() {

  }

  muteFunction() {

  }
}*/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { NgbModal, NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";
import {AuthServiceService} from "../auth/auth-service.service";
import {SearchService} from "../search/search.service";
import {Observable, Subject} from "rxjs";
import {User} from "../models/user/user.model";
import { FormGroup} from "@angular/forms";
import { HttpClient} from "@angular/common/http";
import { VideochatPageService} from "./videochat-page.service";
import { Notification } from '../models/notification.model';

const MESSAGE_TYPE = {
  SDP: 'SDP',
  CANDIDATE: 'CANDIDATE',
};

@Component({
  selector: 'app-videochat',
  templateUrl: './videochat.component.html',
  styleUrls: ['./videochat.component.css']
})
export class VideochatComponent implements OnInit {

  formgroup: FormGroup;
  userChanged = new Subject<User>();

  user = new User(localStorage.getItem('id'),
    localStorage.getItem('name'),
    localStorage.getItem('email'));
  seachedUser: User = new User('', '', '');
  deleteSearchedUser: boolean = true;
  notifications: Notification[];
  notif$ = new Subject<Notification[]>();

  code: any;
  peerConnection: any;
  signaling: any;
  senders: any = [];
  userMediaStream: any;
  len: number;
  users$: Observable<User[]>;
  private searchText$ = new Subject<string>();
  constructor(public auth: AuthServiceService, private route: ActivatedRoute, private searchService: SearchService,
              private config: NgbAccordionConfig, private http: HttpClient, private videoService: VideochatPageService,
              private modalService: NgbModal, private router: Router) { }
  ngOnInit() {
    this.users$ = this.searchText$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(name =>
        this.searchService.search(name))
    );
    if (this.auth.isAuthenticated()) {
      this.searchService.searchedUser.subscribe(u => {
        this.seachedUser = u;
        this.deleteSearchedUser = false;
      });

      this.videoService.getUserNotifications(this.user.id).subscribe(notiList => {
        this.notifications = notiList;
        this.notif$.next(this.notifications);
        this.notif$.subscribe(nl => this.notifications = nl);
      });

      console.log('kab');
      setTimeout(() => {
        console.log(this.user);
        console.log(localStorage.getItem('name'));
        console.log(localStorage.getItem('email'));
        console.log(localStorage.getItem('id'));

        this.user.name = localStorage.getItem('name');
        this.user.email = localStorage.getItem('email');
        this.user.id = localStorage.getItem('id');
        this.user.about = localStorage.getItem('about');

      }, 200);

    }
  }

  onVideoCall(id){
    console.log("id" + id);
    const code = Math.floor(100000000 + Math.random() * 900000000);
    console.log(code);
    this.videoService.videoCallRequest(id, code, this.user.name).subscribe();
    this.router.navigateByUrl('/videocall/' + code);
  }

  onDeleteNotification(id, i){
    this.notifications.splice(i, 1);
    this.notif$.next(this.notifications);
    this.notif$.subscribe(nl => this.notifications = nl);
    this.videoService.deleteNotification(id).subscribe();
  }

  openWindowCustomClass(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  open(content) {
    this.modalService.open(content);
  }

  onDeleteSearchedUser(){
    this.deleteSearchedUser = true;
  }

  search(name: string){
    if (name === '') {
      this.len = 0;
    }
    else {this.len = 1; }
    this.searchText$.next(name);
  }

  clicked(u){
    this.searchService.searchedUser.next(u);
  }


}
