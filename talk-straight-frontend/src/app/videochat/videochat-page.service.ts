import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Notification} from '../models/notification.model';

@Injectable({
  providedIn: 'root'
})
export class VideochatPageService {

  url: string;
  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080';
  }

  videoCallRequest(userId, code, senderName){
    const data = [];
    data.push(userId);
    data.push(code);
    data.push(senderName);
    return this.http.post(this.url + '/videocall', data);
  }

  getUserNotifications(id): Observable<Notification[]>{
    return this.http.get<Notification[]>(this.url + '/notifications/' + id);
  }

  deleteNotification(id){
    return this.http.delete<Notification>(this.url + '/notification/' + id);
  }
}
