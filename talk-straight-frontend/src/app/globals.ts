'use strict';

export const levels = ['Rookie', 'Intermediate', 'Advanced', 'Expert'];
export const specialRewards = ['Busy Bee', 'Poet', 'Graduate'];

export const totalLevels = levels.length;
export const totalSpecialRewards = specialRewards.length;

export let pointScore = 107;
export let levelsAchieved = 2;
export let specialRewardsAchieved = 1;
