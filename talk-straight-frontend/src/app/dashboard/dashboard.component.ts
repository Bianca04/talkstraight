import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material';
import { AchievementsService } from '../achievements/achievements.service';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import * as myGlobals from '../globals';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  primaryColor: ThemePalette = 'primary';
  accentColor: ThemePalette = 'accent';
  today = new Date();
  value = 70;

  totalLevels = myGlobals.totalLevels;
  totalSpecialRewards = myGlobals.totalSpecialRewards;
  levelsAchieved = myGlobals.levelsAchieved;
  specialsAchieved = myGlobals.specialRewardsAchieved;

  currentLevel: string;
  levelUpPoints: number;
  levelUpProgress: number;

  // Doughnut chart
  public doughnutChartOptions: ChartOptions = {
    responsive: true,
  };
  public doughnutChartLabels: Label[] = ['A-Laute', 'B-Laute', 'C-Laute', 'S-Laute', 'Other'];
  public doughnutChartData: SingleDataSet = [[1, 1, 1, 1, 0]];
  public doughnutChartColors: Array<any> = [{
      // all colors in order
      backgroundColor: ['#00838f', '#ff7043', '#fdd835', '#e91e63', '#4caf50']
    }];
  public doughnutChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public doughnutChartPlugins = [{
    beforeInit(chart) {
      chart.legend.afterFit = function() {
        this.height += 10;
      };
    }
  }];

  constructor(private achievementsService: AchievementsService) { }

  ngOnInit() {

    setInterval(() => {
      this.today = new Date();
    }, 1000);

    this.setLevels();
    this.setSpecialRewards();

    const levelUpProgress = this.achievementsService.getLevelUpProgress();
    this.currentLevel = this.achievementsService.getCurrentLevel();
    this.levelUpPoints = levelUpProgress[0];
    this.levelUpProgress = levelUpProgress[1];

  }

  // Rewards achieved

  private setLevels() {
    const parentStar = document.getElementById('parentStar');

    for (let i = 1, counter = 1; i < this.totalLevels; i++, counter++) {
      const childStar = document.getElementById('childStar' + i);

      parentStar.appendChild(childStar.cloneNode(true));
      childStar.setAttribute('id', 'childStar' + (i + 1));

      if (i >= this.levelsAchieved) {
        document.getElementById('childStar' + i).setAttribute('style', 'filter: grayscale(100%)');
      }

    }
  }

  private setSpecialRewards() {
    const parentMedal = document.getElementById('parentMedal');

    for (let i = 1; i < this.totalSpecialRewards; i++) {

      const childMedal = document.getElementById('childMedal' + i);

      parentMedal.appendChild(childMedal.cloneNode(true));

      childMedal.setAttribute('id', 'childMedal' + (i + 1));

      if (i >= this.specialsAchieved) {
        document.getElementById('childMedal' + i).setAttribute('style', 'filter: grayscale(100%)');
      }

      if (i === 2) {
        parentMedal.appendChild(childMedal.cloneNode(false));
        childMedal.setAttribute('id', 'childMedalEmpty');
      }
    }
  }


  // Recordings uploaded (Chart)

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
