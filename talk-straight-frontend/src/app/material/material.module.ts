import { NgModule } from '@angular/core';
import { MatButtonModule, MatInputModule, MatFormFieldModule,
  MatCardModule, MatListModule, MatProgressBarModule,
  MatDatepickerModule, MatTooltipModule, MatDialogModule, MatGridListModule, MatChipsModule} from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMomentDateModule } from '@angular/material-moment-adapter';


const MaterialComponents = [
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatListModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatMomentDateModule,
  MatTooltipModule,
  MatDialogModule,
  MatGridListModule,
  MatChipsModule
];

@NgModule({
  imports: [MaterialComponents],
  exports: [MaterialComponents]
})
export class MaterialModule { }
