INSERT INTO USERS (userid, username, password, email) VALUES (1002,'usertest1', '1234', 'abc@mail.com');
INSERT INTO USERS (userid, username, password, email) VALUES (1003, 'usertest2', '1235', 'abd@mail.com');
INSERT INTO USERS (userid, username, password, email) VALUES (1004, 'usertest3', '1236', 'abe@mail.com');


INSERT INTO CATEGORIES (categoryid, categoryname) VALUES (1002, 'A-Laute');
INSERT INTO CATEGORIES (categoryid, categoryname) VALUES (1003, 'B-Laute');
INSERT INTO CATEGORIES (categoryid, categoryname) VALUES (1004, 'C-Laute');
INSERT INTO CATEGORIES (categoryid, categoryname) VALUES (1005, 'S-Laute');

INSERT INTO RECORDINGS (RECORDINGID, RECORDINGNAME, URL, CATEGORYID, USERID) VALUES (1002, 'A-Laute.txt', 'uploads\A-Laute\A-Laute.txt', 1002, 1002) ;
INSERT INTO RECORDINGS (RECORDINGID, RECORDINGNAME, URL, CATEGORYID, USERID) VALUES (1003, 'B-Laute.txt', 'uploads\B-Laute\B-Laute.txt', 1003, 1002) ;
INSERT INTO RECORDINGS (RECORDINGID, RECORDINGNAME, URL, CATEGORYID, USERID) VALUES (1004, 'C-Laute.txt', 'uploads\C-Laute\C-Laute.txt', 1004, 1002);
INSERT INTO RECORDINGS (RECORDINGID, RECORDINGNAME, URL, CATEGORYID, USERID) VALUES (1005, 'S-Laute.txt', 'uploads\S-Laute\S-Laute.txt', 1005, 1002);



