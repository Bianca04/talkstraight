package talk.straight.Services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import talk.straight.DTOs.CategoryDTO;
import talk.straight.DTOs.RecordingsDTO;
import talk.straight.DTOs.UsersDTO;
import talk.straight.Repositories.CategoryJpaRepository;
import talk.straight.Repositories.RecordingJpaRepository;
import talk.straight.Repositories.UserJpaRepository;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

    public static final Logger logger = LoggerFactory.getLogger(FilesStorageService.class);
    private final Path root = Paths.get("uploads");
    private UsersDTO loggedinUser;

    //vor Testing
    private final Path a = root.resolve("A-Laute");
    private final Path b = root.resolve("B-Laute");
    private final Path c = root.resolve("C-Laute");
    private final Path s = root.resolve("S-Laute");

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private RecordingJpaRepository recordingsJpaRepository;

    @Autowired
    private CategoryJpaRepository categoryJpaRepository;

    @Override
    public void init() {
        try {
            Files.createDirectory(root);
            Files.createDirectory(a);
            File file = new File(a + "/A-Laute.txt");
            file.getParentFile().mkdirs();
            file.createNewFile();
            Files.createDirectory(b);
            file = new File(b + "/B-Laute.txt");
            file.getParentFile().mkdirs();
            file.createNewFile();
            Files.createDirectory(c);
            file = new File(c + "/C-Laute.txt");
            file.getParentFile().mkdirs();
            file.createNewFile();
            Files.createDirectory(s);
            file = new File(s + "/S-Laute.txt");
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    @Override
    public void saveWithCategory(MultipartFile file, String category) {
        try {
            Path categoryPath = root.resolve(category);
            CategoryDTO categoryDTO = categoryJpaRepository.getByName(category);
            if(categoryDTO == null) {
                Files.createDirectory(categoryPath);
                categoryDTO = new CategoryDTO(category);
                categoryJpaRepository.save(categoryDTO);
            }
            Files.copy(file.getInputStream(), categoryPath.resolve(file.getOriginalFilename()));
            long id = 1002;
            loggedinUser = userJpaRepository.findById(id).get();
            RecordingsDTO recordingsDTO = new RecordingsDTO(file.getOriginalFilename());
            logger.info("recordingsDTO created" + recordingsDTO.getName());
            recordingsDTO.setCategory(categoryDTO);
            recordingsDTO.setUser(loggedinUser);
            recordingsDTO.setUrl(categoryPath.resolve(file.getOriginalFilename()).toString());
            recordingsJpaRepository.save(recordingsDTO);
            logger.info("saving done");
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                logger.info("resource: " + resource.getFilename());
                return resource;
            } else {
                logger.info("resource does not exist");
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            logger.info("try went wrong");
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public Stream<Path> loadAllByCategory(String categoryName) {
        try {
            Path categoryPath = root.resolve(categoryName);
            return Files.walk(categoryPath, 1).filter(path -> !path.equals(categoryPath)).map(categoryPath::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }


}
