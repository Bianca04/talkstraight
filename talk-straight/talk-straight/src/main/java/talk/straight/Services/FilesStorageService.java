package talk.straight.Services;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import talk.straight.DTOs.CategoryDTO;

public interface FilesStorageService {
    public void init();

    public Resource load(String filename);

    public void deleteAll();

    public Stream<Path> loadAllByCategory(String categoryname);

    public void saveWithCategory(MultipartFile file, String category);
}
