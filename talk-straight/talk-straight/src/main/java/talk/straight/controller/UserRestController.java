package talk.straight.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import talk.straight.DTOs.NotificationDTO;
import talk.straight.DTOs.RecordingsDTO;
import talk.straight.DTOs.UsersDTO;
import talk.straight.Exceptions.CustomErrors;
import talk.straight.Repositories.NotificationRepository;
import talk.straight.Repositories.UserJpaRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@Component
public class UserRestController {

	public static final Logger logger = LoggerFactory.getLogger(UserRestController.class);

	private UsersDTO user;

	@Autowired
	private UserJpaRepository userJpaRepository;
	
	@Autowired
	private NotificationRepository notifJpaRepo;
	
	@GetMapping("/")
	public ResponseEntity<List<UsersDTO>> getAllUser(){
		List<UsersDTO> users = userJpaRepository.findAll();
		return new ResponseEntity<List<UsersDTO>>(users,HttpStatus.ACCEPTED);
	}

	@PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UsersDTO> createUser(@Valid @RequestBody UsersDTO user){
		logger.info("Creating User: {}", user);
		if(userJpaRepository.findByEmail(user.getEmail())!=null) {
			logger.error("Email {} already exist",user.getEmail());
			return new ResponseEntity<UsersDTO>(new CustomErrors("User already exist"),HttpStatus.CONFLICT);
		}
		userJpaRepository.save(user);
		this.user = user;
		return new ResponseEntity<UsersDTO>(user, HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UsersDTO> login(@Valid @RequestBody UsersDTO user){
		if(userJpaRepository.findByEmail(user.getEmail())== null || userJpaRepository.findByEmailPassword(user.getEmail(), user.getPassword()) == null) {
			logger.error("user not found");
			return new ResponseEntity<UsersDTO>(new CustomErrors("User Not Found"),HttpStatus.NOT_FOUND);
		}
		UsersDTO u = userJpaRepository.findByEmail(user.getEmail());
		this.user = u;
		// hier vermutlich etwas setzen, um im Recordings zu wissen welcher User eingeloggt ist...
		return new ResponseEntity<UsersDTO>(u,HttpStatus.ACCEPTED);
	}
	
	@PostMapping(value = "/{otp}", consumes = MediaType.ALL_VALUE)
	public ResponseEntity<UsersDTO> verifyOtp(@PathVariable("otp") String otp, @RequestBody UsersDTO user){
		
		if(otp.equals("1001")) {
			System.out.println(otp);
			userJpaRepository.save(this.user);
			return new ResponseEntity<UsersDTO>(user,HttpStatus.OK);
		}
		return new ResponseEntity<UsersDTO>(HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/users/{name}")
	public ResponseEntity<List<UsersDTO>> searchUsers(@PathVariable("name") String name){
		List<UsersDTO> users = userJpaRepository.findAllByName(name);
		return new ResponseEntity<List<UsersDTO>>(users, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/getUser/{id}")
	public ResponseEntity<UsersDTO> getUser(@PathVariable("id") Long id){
		UsersDTO user = ((Optional<UsersDTO>)userJpaRepository.findById(id)).get();
		return new ResponseEntity<UsersDTO>(user, HttpStatus.OK);
		
	}
	
	@PostMapping("/videocall")
	public void videoCallRequest(@Valid @RequestBody String data[]) {
		System.out.println(data[0] + " " + data[1] + " " + data[2]);
		NotificationDTO newNotif = new NotificationDTO();
		newNotif.setCode(data[1]);
		UsersDTO u = new UsersDTO();
		u.setId(Long.parseLong(data[0]));
		newNotif.setUser(u);
		newNotif.setSenderName(data[2]);
		notifJpaRepo.save(newNotif);
	}
	
	@GetMapping("/notifications/{id}")
	public ResponseEntity<List<NotificationDTO>> getNotifications(@PathVariable("id") String id){
		List<NotificationDTO> notifList = notifJpaRepo.findByUserId(Long.parseLong(id));
		return new ResponseEntity<List<NotificationDTO>>(notifList, HttpStatus.OK);
	}
	
	@DeleteMapping("/notification/{id}")
	public ResponseEntity<NotificationDTO> deleteNotification(@PathVariable("id") Long id){
		notifJpaRepo.deleteById(id);
		return new ResponseEntity<NotificationDTO>(HttpStatus.NO_CONTENT);
	}
	
}
