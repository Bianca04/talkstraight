package talk.straight.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import talk.straight.DTOs.CategoryDTO;
import talk.straight.DTOs.RecordingsDTO;
import talk.straight.Message.ResponseMessage;
import talk.straight.Repositories.CategoryJpaRepository;
import talk.straight.Repositories.RecordingJpaRepository;
import talk.straight.Services.FilesStorageService;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/")
public class FilesController {
    public static final Logger logger = LoggerFactory.getLogger(FilesController.class);

    @Autowired
    FilesStorageService storageService;

    @Autowired
    private RecordingJpaRepository recordingJpaRepository;

    @Autowired
    private CategoryJpaRepository categoryJpaRepository;

    @PostMapping("/uploadCategory/{categoryname:.+}")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String categoryname) {
        String message = "";
        try {
            logger.info("vor save CategoryUpload: " + file.getOriginalFilename());
            storageService.saveWithCategory(file, categoryname);
            logger.info("nach save CategoryUpload: " + file.getOriginalFilename());

            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        logger.info(filename);
        Resource file = storageService.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/filesCategory/{category:.+}")
    @ResponseBody
    public ResponseEntity<List<RecordingsDTO>> getFilesForCategory(@PathVariable String category) {
        List<RecordingsDTO> fileEntities = storageService.loadAllByCategory(category).map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
            logger.info(url);
            RecordingsDTO record = recordingJpaRepository.findByFilename(filename);
            record.setUrl(url);
            return record;
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(fileEntities);
    }

    @GetMapping("/categories")
    @ResponseBody
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        List<CategoryDTO> categories = categoryJpaRepository.findAll();
        return new ResponseEntity<>(categories, HttpStatus.ACCEPTED);
    }
}
