package talk.straight;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import talk.straight.Services.FilesStorageService;
import javax.annotation.Resource;

@SpringBootApplication
public class TalkstraightApplication {

	@Resource
	FilesStorageService storageService;

	public static void main(String[] args) {
		SpringApplication.run(TalkstraightApplication.class, args);
	}

	@Bean
	CommandLineRunner init() {
		storageService.deleteAll();
		storageService.init();

		return null;
	}

}
