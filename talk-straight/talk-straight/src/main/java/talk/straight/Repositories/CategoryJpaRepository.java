package talk.straight.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import talk.straight.DTOs.CategoryDTO;


@Repository
public interface CategoryJpaRepository extends JpaRepository<CategoryDTO, Long> {


    @Query(value = "SELECT * from categories where categoryname = ?1", nativeQuery = true)
    CategoryDTO getByName(String categoryname);
}
