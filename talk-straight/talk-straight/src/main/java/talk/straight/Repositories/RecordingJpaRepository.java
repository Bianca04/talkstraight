package talk.straight.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import talk.straight.DTOs.RecordingsDTO;

import java.util.List;

@Repository
public interface RecordingJpaRepository extends JpaRepository<RecordingsDTO, Long> {


    @Query(value = "SELECT * from recordings where recordingname = ?1", nativeQuery = true)
    RecordingsDTO findByFilename(String filename);
    @Query(value = "SELECT * from recordings where userid = ?1", nativeQuery = true)
    List<RecordingsDTO> findAllByUser(Long userid);
    @Query(value = "SELECT * from recordings where categoryid = ?1", nativeQuery = true)
    List<RecordingsDTO> findAllByCategory(Long categoryid);
}
