package talk.straight.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import talk.straight.DTOs.UsersDTO;

import java.util.List;

@Repository
public interface UserJpaRepository extends JpaRepository<UsersDTO, Long> {

	UsersDTO findByEmail(String email);
	@Query(value = "SELECT * from users where email = ?1 and password = ?2", nativeQuery = true)
	UsersDTO findByEmailPassword(String email, String password);
	@Query(value = "SELECT * from users where username LIKE ?1%", nativeQuery = true)
	List<UsersDTO> findAllByName(String username);
}
