package talk.straight.DTOs;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class UsersDTO {
	
	public UsersDTO() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator = "user_id_seq_generator")
	@SequenceGenerator(name = "user_id_seq_generator", sequenceName = "user_seq")
	@Column(name = "userid")
	private long id;
	
	@Length(max = 145)
	@Column(name = "username")
	private String name;
	
	@Length(max = 145)
	@Column(name = "email")
	private String email;
	
	@Length(max = 100)
	@Column(name = "password")
	private String password;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UsersDTO [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + "]";
	}
	
	
	
}
