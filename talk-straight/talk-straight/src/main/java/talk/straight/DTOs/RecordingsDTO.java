package talk.straight.DTOs;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table(name = "recordings")
public class RecordingsDTO {

    public RecordingsDTO() {
        super();
    }

    public RecordingsDTO(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "recordingid_seq_generator")
    @SequenceGenerator(name = "recordingid_seq_generator", sequenceName = "recording_seq")
    @Column(name = "recordingid")
    private long id;


    @Length(max = 145)
    @Column(name = "recordingname")
    private String name;

    @ManyToOne
    @JoinColumn(name = "userid")
    private UsersDTO user;

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    @ManyToOne
    @JoinColumn(name = "categoryid")
    private CategoryDTO category;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Length(max = 500)
    @Column(name = "url")
    private String url;


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UsersDTO getUser() {
        return user;
    }

    public void setUser(UsersDTO user) {
        this.user = user;
    }

    public String toString() {
        return "RecordingsDTO [id=" + id + ", name=" + name + ", user=" + user.getName() + ", url=" + url + ", category=" + category.getName() + "]";
    }
}

