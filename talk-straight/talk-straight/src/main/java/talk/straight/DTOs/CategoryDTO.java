package talk.straight.DTOs;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table(name = "categories")
public class CategoryDTO {

    public CategoryDTO() {
        super();
    }

    public CategoryDTO(String name)  {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Length(max = 145)
    @Column(name = "categoryname")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "categoryid_seq_generator")
    @SequenceGenerator(name = "categoryid_seq_generator", sequenceName = "category_seq")
    @Column(name = "categoryid")
    private long id;
}
