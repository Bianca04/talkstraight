package talk.straight.Exceptions;

public class NoObjectFoundByIdException extends Exception {
    public NoObjectFoundByIdException(String errorMessage) {
        super(errorMessage);
    }
}
