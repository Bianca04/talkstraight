package talk.straight.Exceptions;


import talk.straight.DTOs.UsersDTO;

public class CustomErrors extends UsersDTO {

	private String error;
	
	public CustomErrors(String err) {
		error = err;
	}

	public String getError() {
		return error;
	}
}
